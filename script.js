let image=document.querySelectorAll(".image-to-show")
image.forEach(el=>{el.classList.add("hidden")})
let i =0
let timer
let buttonStart=document.getElementById("play")
let buttonStop =document.getElementById("stop")
buttonStart.disabled = "false"
let tick = function (){
       
    if (i<image.length-1){
       image[i].classList.remove("hidden")}
       if(i>=1){
       image[i-1].classList.add("hidden")}
       if (i==image.length-1){
           image[i].classList.remove("hidden")  
       }
       if (i>image.length-1){
           i=0
           image[0].classList.toggle("hidden")
       }
       i++
       timer =setTimeout(tick,3000)
    }

 timer = setTimeout(tick,0)

function pause() {
    clearTimeout(timer);
    buttonStart.disabled = false;
    buttonStop.disabled = true;    
 }

buttonStop.addEventListener("click",pause)

function start() {
    timer = setTimeout(tick,3000);
    buttonStart.disabled = true;
    buttonStop.disabled = false;
 } 

 buttonStart.addEventListener("click",start)